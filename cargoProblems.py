from aipython.stripsProblem import STRIPS_domain, Strips, Planning_problem
from cargoDomain import *
from cargoProblem import *

###### Cargo Problem ######

#Problem 1
# Cargos are at KRK and should be moved to WAW.
# Planes are at KRK and WAW. Planes should be moved to the opposite airport at the end.

# Problem 2
# Cargos are at KRK and WAW. Cargo from WAW should be moved to KRK while the cargo from KRK should be moved to Plane 1.
# Both planes start at GDN and should end at GDN.

# Problem 3
# Cargos are at WAW and should be moved to GDN and KRK. 
# Planes are at GDN and KRK and should end at WAW. 


#### Implementations ####

cargos = ['Cargo_1', 'Cargo_2']
planes = ['Plane_1', 'Plane_2']
airports = ['KRK', 'WAW', 'GDN']

cargo_domain = CargoDomain(cargos, planes, airports)

# Problem 1
# initial state
# Plane 1 at KRK
# Plane 2 at WAW
# Cargo 1 at KRK
# Cargo 2 at KRK

# goal state
# Plane 1 at WAW
# Plane 2 at KRK
# Cargo 1 at WAW
# Cargo 2 at WAW

problem1_initial = {
    is_at('Cargo_1', 'KRK'): True,
    is_at('Cargo_2', 'KRK'): True,
    is_at('Plane_1', 'KRK'): True,
    is_at('Plane_2', 'WAW'): True
}

problem1_goal = {
    is_at('Cargo_1', 'WAW'): True,
    is_at('Cargo_2', 'WAW'): True,
    is_at('Plane_1', 'WAW'): True,
    is_at('Plane_2', 'KRK'): True
}

problem1 = CargoProblem(cargo_domain, [problem1_initial, problem1_goal])

# initial state
# Plane 1 at GDN
# Plane 2 at GDN
# Cargo 1 at KRK
# Cargo 2 at WAW

# goal state
# Plane 1 at GDN
# Plane 2 at GDN
# Cargo 1 in Plane 1
# Cargo 2 at KRK

problem2_initial = {
    is_at('Cargo_1', 'KRK'): True,
    is_at('Cargo_2', 'WAW'): True,
    is_at('Plane_1', 'GDN'): True,
    is_at('Plane_2', 'GDN'): True
}

problem2_goal = {
    is_in('Cargo_1', 'Plane_1'): True,
    is_at('Cargo_2', 'KRK'): True,
    is_at('Plane_1', 'GDN'): True,
    is_at('Plane_2', 'GDN'): True
}

problem2 = CargoProblem(cargo_domain, [problem2_initial, problem2_goal])

# initial state
# Plane 1 at GDN
# Plane 2 at KRK
# Cargo 1 at WAW
# Cargo 2 at WAW

# goal state
# Plane 1 at WAW
# Plane 2 at WAW
# Cargo 1 at GDN
# Cargo 2 at KRK

problem3_initial = {
    is_at('Cargo_1', 'WAW'): True,
    is_at('Cargo_2', 'WAW'): True,
    is_at('Plane_1', 'GDN'): True,
    is_at('Plane_2', 'KRK'): True
}

problem3_goal = {
    is_at('Cargo_1', 'GDN'): True,
    is_at('Cargo_2', 'KRK'): True,
    is_at('Plane_1', 'WAW'): True,
    is_at('Plane_2', 'WAW'): True
}

problem3 = CargoProblem(cargo_domain, [problem3_initial, problem3_goal])



### Problems with subgoals

# Problem 1 with subgoals

# Cargos are at KRK and should be moved to WAW.
# Planes are at KRK and WAW. Planes should be moved to the opposite airport at the end.

# first intermediate goal: Cargo 1 should be moved to GDN
# second intermediate goal: Cargo 1 should be moved back to KRK, Plane 1 should move to WAW

problem1_intermediate1 = {
    is_at('Cargo_1', 'GDN'): True,
    is_at('Cargo_2', 'KRK'): True,
    is_at('Plane_1', 'KRK'): True,
    is_at('Plane_2', 'WAW'): True
}

problem1_intermediate2 = {
    is_at('Cargo_1', 'KRK'): True,
    is_at('Cargo_2', 'KRK'): True,
    is_at('Plane_1', 'WAW'): True,
    is_at('Plane_2', 'WAW'): True
}

problem1_with_subgoals = CargoProblem(cargo_domain, [problem1_initial, problem1_intermediate1, problem1_intermediate2, problem1_goal])

# Problem 2 with subgoals

# Cargos are at KRK and WAW. Cargo from WAW should be moved to KRK while the cargo from KRK should be moved to Plane 1.
# Both planes start at GDN and should end at GDN.

# first intermediate goal: Both planes should move to KRK
# second intermediate goal: Both planes should move to WAW, Cargo 2 should be moved to KRK

problem2_intermediate1 = {
    is_at('Cargo_1', 'KRK'): True,
    is_at('Cargo_2', 'WAW'): True,
    is_at('Plane_1', 'KRK'): True,
    is_at('Plane_2', 'KRK'): True
}

problem2_intermediate2 = {
    is_at('Cargo_1', 'KRK'): True,
    is_at('Cargo_2', 'KRK'): True,
    is_at('Plane_1', 'WAW'): True,
    is_at('Plane_2', 'WAW'): True
}

problem2_with_subgoals = CargoProblem(cargo_domain, [problem2_initial, problem2_intermediate1, problem2_intermediate2, problem2_goal])

# Problem 3 with subgoals

# Cargos are at WAW and should be moved to GDN and KRK. 
# Planes are at GDN and KRK and should end at WAW. 

# first intermediate goal: Both cargos should be moved to GDN
# second intermediate goal: Cargo 1 and Plane 1 should be moved to KRK

problem3_intermediate1 = {
    is_at('Cargo_1', 'GDN'): True,
    is_at('Cargo_2', 'GDN'): True,
    is_at('Plane_1', 'GDN'): True,
    is_at('Plane_2', 'KRK'): True
}

problem3_intermediate2 = {
    is_at('Cargo_1', 'KRK'): True,
    is_at('Cargo_2', 'GDN'): True,
    is_at('Plane_1', 'KRK'): True,
    is_at('Plane_2', 'KRK'): True
}

problem3_with_subgoals = CargoProblem(cargo_domain, [problem3_initial, problem3_intermediate1, problem3_intermediate2, problem3_goal])
