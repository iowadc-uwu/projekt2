from typing import Iterable, Callable

from aipython.stripsProblem import STRIPS_domain, Strips, Planning_problem


def is_at(obj, airport):
    return f'{obj}_is_at_{airport}'

def is_in(cargo, plane):
    return f'{cargo}_is_in_{plane}'

def move_name_load(cargo, plane, airport):
    return f'{cargo}_load_{plane}_from_{airport}'

def move_name_unload(cargo, plane, airport):
    return f'{cargo}_unload_{plane}_from_{airport}'

def move_name_fly(plane, from_airport, to_airport):
    return f'{plane}_fly_from_{from_airport}_to_{to_airport}'

def create_cargo_domain(cargos: set[str], planes: set[str], airports: set[str]) -> STRIPS_domain:
    # load
    stmap = {
        Strips(
            move_name_load(cargo, plane, airport),
            {
                is_at(cargo, airport): True,
                is_at(plane, airport): True
            },
            {
                is_in(cargo, plane): True,
                is_at(cargo, airport): False
            }
        )
        for cargo in cargos for plane in planes for airport in airports
    }
    # unload
    stmap.update({
        Strips(
            move_name_unload(cargo, plane, airport),
            {
                is_in(cargo, plane): True,
                is_at(plane, airport): True
            },
            {
                is_at(cargo, airport): True,
                is_in(cargo, plane): False
            }
        )
        for cargo in cargos for plane in planes for airport in airports
    })
    # fly
    stmap.update({
        Strips(
            move_name_fly(plane, from_airport, to_airport),
            {
                is_at(plane, to_airport): False,
                is_at(plane, from_airport): True
            },
            {
                is_at(plane, to_airport): True,
                is_at(plane, from_airport): False
            }
        )
        for plane in planes for from_airport in airports for to_airport in airports
    })

    feature_domain_dict = {is_at(obj, airport): {True, False} for obj in cargos | planes for airport in airports}
    feature_domain_dict.update({is_in(cargo, plane): {True, False} for cargo in cargos for plane in planes})

    return STRIPS_domain(feature_domain_dict, stmap)
    

class CargoDomain:
    domain: STRIPS_domain

    airports: set[str] = {}
    planes: set[str] = {}
    cargos: set[str] = {}

    def __init__(self, cargos: Iterable[str] = set(), planes: Iterable[str] = set(), airports: Iterable[str] = set()):
        self.cargos = set(cargos)
        self.planes = set(planes)
        self.airports = set(airports)
        self.domain = create_cargo_domain(self.cargos, self.planes, self.airports)
    
    def create_heuristic(self) -> Callable[[dict, dict], int]:
        def heuristic(state: dict, goal: dict) -> int:
            heur = 0

            for cargo in self.cargos:
                state_plane = self.get_cargo_current_plane(state, cargo)
                goal_plane = self.get_cargo_current_plane(goal, cargo)
                state_airport = self.get_obj_current_airport(state, cargo)
                goal_airport = self.get_obj_destination_airport(goal, cargo)

                if self.is_cargo_in_any_plane(state, cargo):
                    if self.is_cargo_in_any_plane(goal, cargo):
                        if state_plane != goal_plane:
                            # need to unload, load
                            heur += 2
                    else:
                        # need to unload
                        heur += 1
                else:
                    if self.is_cargo_in_any_plane(goal, cargo):
                        # need to load
                        heur += 1
                    else:
                        if state_airport != goal_airport:
                            # need to load and unload
                            heur += 2

            return heur
        
        return heuristic

    def is_cargo_in_any_plane(self, state: dict, cargo: str) -> bool:
        return any([state.get(is_in(cargo, plane), False) for plane in self.planes])

    def get_obj_current_airport(self, state: dict, obj: str) -> str | None:
        for airport in self.airports:
            if state.get(is_at(obj, airport), False):
                return airport
        return None

    def get_cargo_current_plane(self, state: dict, cargo: str) -> str | None:
        for plane in self.planes:
            if state.get(is_in(cargo, plane), False):
                return plane
        return None

    def get_obj_destination_airport(self, state: dict, obj: str) -> str | None:
        for airport in self.airports:
            if state.get(is_at(obj, airport), False):
                return airport
        return None

    def get_cargo_destination_plane(self, state: dict, cargo: str) -> str | None:
        for plane in self.planes:
            if state.get(is_in(cargo, plane), False):
                return plane
        return None

    def are_planes_in_one_airport(self, state: dict, plane1: str, plane2: str) -> bool:
        return self.get_obj_current_airport(state, plane1) == self.get_obj_current_airport(state, plane2)

