import pandas as pd

from aipython.searchProblem import Path

from cargoDomain import *


class CargoProblem:
    cargo_domain: CargoDomain
    initial_state: dict
    goal_state: dict
    subproblems: list[Planning_problem] 

    def __init__(self, cargo_domain: CargoDomain, intermediate_states: list[dict] = list()):
        self.cargo_domain = cargo_domain
        self.subproblems = list()

        base_state = {
            is_at(obj, airport): False
            for obj in self.cargo_domain.cargos | self.cargo_domain.planes for airport in self.cargo_domain.airports
        }
        base_state.update({
            is_in(cargo, plane): False
            for cargo in self.cargo_domain.cargos for plane in self.cargo_domain.planes
        })
        
        for i in range(len(intermediate_states) - 1):
            initial_state = base_state | intermediate_states[i]
            goal_state = base_state | intermediate_states[i + 1]
            print(initial_state)
            print(goal_state)
            self.subproblems.append(Planning_problem(self.cargo_domain.domain, initial_state, goal_state))

        self.initial_state = base_state | intermediate_states[0]
        self.goal_state = base_state | intermediate_states[-1]


    def solve(self, Planner, Searcher, use_heuristic: bool) -> list[Path]:
        subproblems_solutions: list[Path] = list()

        for subproblem in self.subproblems:
            if use_heuristic:
                planner = Planner(subproblem, self.cargo_domain.create_heuristic())
            else:
                planner = Planner(subproblem)

            searcher = Searcher(planner)

            subproblem_solution: Path = searcher.search()
            subproblems_solutions.append(subproblem_solution)
        
        return subproblems_solutions

    def to_latex_table(self, solution: list[Path]) -> str:
        actions = []
        for i, path in enumerate(solution):
            rec = path
            subproblem_actions = []
            while rec is not None and rec.arc is not None:
                subproblem_actions.append(repr(rec.arc.action))
                rec = rec.initial
            subproblem_actions.reverse()
            actions += subproblem_actions

            if i == len(solution) - 1:
                actions.append("\\textbf{Osiągnięto cel}")
            else:
                actions.append(f"\\textbf{{Osiągnięto podcel {i+1}}}")
        
        print(actions)

        df = pd.DataFrame([[action.replace("_", " ")] for action in actions], columns=["Akcje"])

        latex_table = df.to_latex(index=False)
        # add centering
        return "\\begin{center}\n" + latex_table + "\\end{center}"

