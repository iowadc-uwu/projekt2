import pandas as pd
from time import time

from aipython.stripsProblem import STRIPS_domain, Strips, Planning_problem
from aipython.stripsForwardPlanner import Forward_STRIPS
from aipython.searchBranchAndBound import DF_branch_and_bound
from aipython.searchMPP import SearcherMPP
from aipython.stripsHeuristic import *
from aipython.searchGeneric import *

from cargoProblems import *
from cargoProblem import *


time1 = time()
res1 = problem2_with_subgoals.solve(Forward_STRIPS, SearcherMPP, True)
time2 = time()

print(f"Time: {time2 - time1}")
print(problem2_with_subgoals.to_latex_table(res1))
