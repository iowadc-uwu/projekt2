# Projekty — STRIPS
### Zadania na 6 punktów:
1. Wybierz dziedzinę STRIPS i trzy przykładowe problemy z co najmniej 50 stanami,
np. stąd: https://github.com/primaryobjects/strips , gdzie rozwiązanie składa się z
minimum 4 instancji akcji. Zdefiniuj je za pomocą STRIPS_domain i
Planning_problem z AIPython: https://artint.info/AIPython/ .
2. Spróbuj rozwiązać problem za pomocą metody forward planning (dla dużych
problemów można zrobić timeout na 5 minut). Zanotuj znalezione rozwiązanie jako
ciąg wykonywanych akcji.
3. Zaproponuj heurystykę do problemu. Opisz jak działa i dlaczego sądzisz, że będzie
pomocna. Rozwiąż problem z heurystyką i zanotuj czas rozwiązania problemu z
heurystyką (tu wymagane jest, aby rozwiązanie zostało znalezione).
Różne grupy mogą pracować z tymi samymi dziedzinami, ale nie z tymi samymi
problemami.
### Zadania na 8 punktów:
1. Wszystkie zadania na 6 punktów.
2. Zdefiniuj podcele dla problemów (minimum dwa podcele do każdego problemu).
Rozwiąż ponownie problem z podcelami, z heurystyką i bez, analogicznie jak w
zadaniach na 6 punktów.
### Zadania na 10 punktów:
1. Wszystkie zadania na 8 punktów.
2. Zdefiniuj i rozwiąż dodatkowo trzy problemy z podcelami których rozwiązanie
wymaga minimum 20 instancji akcji.

Rozwiązania (kod i sprawozdanie) należy przesłać na platformie MS TEAMS do dnia
2024-03-24, godzina 23:59.